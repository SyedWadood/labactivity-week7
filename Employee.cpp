#include <iostream>
#include <cstring>
using namespace std;

struct Employee{
  string F_N;
  string L_N;
  string email;
  string ID;
  string ph_num;
  double salary;
  Employee* Manager;
  
}Mg,E1,E2;


int main()
{

struct Employee Mg,E1,E2;  

Mg.F_N = "Syed";
Mg.L_N = "Hasan";
Mg.email = "SS3858@live.mdx.ac.uk";
Mg.ID = "M00735231";
Mg.ph_num = "0547225604";
Mg.salary = 100000.00;
Mg.Manager = &Mg;

cout << "Manager:- " << "\n" <<
        "First Name: " << Mg.F_N << "\n"<<
        "Last Name: " << Mg.L_N << "\n"<<
        "Email: " << Mg.email << "\n" <<
        "ID: " << Mg.ID << "\n"<<
        "Phone Number: " << Mg.ph_num << "\n"<<
        "Salary: " << Mg.salary << "\n\n";
  // << "Type Employee: " << M.Manager << "\n\n";


E1.F_N = "Anthon";
E1.L_N = "Keppler";
E1.email = "antkep8@live.mdx.ac.uk";
E1.ID = "M00734752";
E1.ph_num = "057123456";
E1.salary = 33000.00;
E1.Manager = &Mg;

cout << "Employee 1:- " << "\n" <<
        "First Name: " << E1.F_N << "\n"<<
        "Last Name: " << E1.L_N << "\n"<<
        "Email: " << E1.email << "\n" <<
        "ID: " << E1.ID << "\n"<<
        "Phone Number: " << E1.ph_num << "\n"<<
        "Salary: " << E1.salary << "\n" <<
        "Employee's Manager:- " << E1.Manager -> F_N   << "\n\n";


E2.F_N = "John";
E2.L_N = "Parr";
E2.email = "JP@live.mdx.ac.uk";
E2.ID = "M00732412";
E2.ph_num = "057123523";
E2.salary = 33000.00;
E2.Manager = &Mg;

cout << "Employee 2:- " << "\n" <<
        "First Name: " << E2.F_N << "\n"<<
        "Last Name: " << E2.L_N << "\n"<<
        "Email: " << E2.email << "\n" <<
        "ID: " << E2.ID << "\n"<<
        "Phone Number: " << E2.ph_num << "\n"<<
        "Salary: " << E2.salary << "\n"<<
        "Employee's Manager:- " << E2.Manager -> F_N << "\n\n";


 return 0;
}

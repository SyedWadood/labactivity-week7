#include <iostream>
using namespace std;


int main(){
  double num = 2.4;
  double*ptr_double = &num;

  cout<<" Variable value: " << num << " ; " << "address: " << &num << "\n";
  cout<<" Pointer value: " << ptr_double << "\n";
  cout<<" address: " << &ptr_double << "\n";
  cout<<" dereference: " << *ptr_double;

  return 0;


}  

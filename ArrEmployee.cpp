#include <iostream>
#include <cstring>
using namespace std;

struct Employee{
  string F_N;
  string L_N;
  string email;
  string ID;
  string ph_num;
  double salary;
  Employee* Manager;
  
};


int main()
{
 int RollNo;  
 cout << "Please Enter the no.of Employees" << "\n" ;
 cin >> RollNo;
 struct Employee Ep[RollNo];

 cout << "Enter the details of every " << RollNo << " employee's" << "\n" ;
 
 for(int i = 0; i < RollNo; i++){
   cout << "Please Enter Your First Name: " << "\n";
     cin >> Ep[i].F_N;
   cout << "Please Enter Your Last Name: " << "\n";
     cin >> Ep[i].L_N;
   cout << "Please Enter Your Email: " << "\n";
     cin >> Ep[i].email;
   cout << "Please Enter Your ID: " << "\n";
     cin >> Ep[i].ID;
   cout << "Please Enter Your Phone Number: " << "\n";
     cin >> Ep[i].ph_num;
   cout << "Please Enter Your Annual Earning: " << "\n";
     cin >> Ep[i].salary;
   cout <<"\n" << "Done!" <<"\n";  
 }  

 cout <<("Employee's Information:- ");
 for(int i = 0; i < RollNo; i++){
   cout << "\n" << "ID : " << Ep[i].ID << "\t" << "Name: " << Ep[i].F_N << Ep[i].L_N;
 }
 
 return 0;
}
